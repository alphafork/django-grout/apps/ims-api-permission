from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import APIGroupPermissionAPI, ContentTypeAPI, GroupPermissionsAPI

router = routers.SimpleRouter()
router.register(r"content-type", ContentTypeAPI, "content-type")
router.register(r"group-permissions", GroupPermissionsAPI, "group-permissions")
router.register(r"api-group-permission", APIGroupPermissionAPI, "api-group-permission")

urlpatterns = [
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
