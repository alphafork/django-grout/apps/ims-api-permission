from django.apps import AppConfig


class IMSAPIPermissionConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_api_permission"

    model_strings = {
        "API_TYPE": "APIType",
        "API_METHOD": "APIMethod",
        "API_GROUP_PERMISSION": "APIGroupPermission",
        "API_USER_BLOCK_LIST": "APIUserBlockList",
    }
