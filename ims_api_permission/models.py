from django.contrib.auth.models import Group, User
from django.db import models


class APIType(models.Model):
    app_label = models.CharField(max_length=255, null=True, blank=True)
    api = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_created_by",
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_updated_by",
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.app_label} | {self.api}"


class APIMethod(models.Model):
    METHODS = [
        ("options", "OPTIONS"),
        ("get", "GET"),
        ("post", "POST"),
        ("put", "PUT"),
        ("patch", "PATCH"),
        ("delete", "DELETE"),
    ]
    api_type = models.ForeignKey(APIType, on_delete=models.CASCADE)
    method = models.CharField(max_length=255, choices=METHODS)
    codename = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_created_by",
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_updated_by",
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.api_type} | {self.method}"


class APIGroupPermission(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    api_method = models.ForeignKey(APIMethod, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_created_by",
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_updated_by",
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.group} | {self.api_method}"


class APIUserBlockList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    api_method = models.ForeignKey(APIMethod, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_created_by",
        blank=True,
        null=True,
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name="%(app_label)s_%(class)s_updated_by",
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.user} | {self.api_method}"
