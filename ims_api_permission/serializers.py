from django.contrib.auth.models import ContentType, Group, Permission
from rest_framework import serializers

from ims_api_permission.models import APIGroupPermission


class APIGroupPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = APIGroupPermission
        exclude = ["created_at", "created_by", "updated_at", "updated_by"]
        extra_meta = {
            "group": {
                "related_model_url": "/user/groups",
            }
        }


class ModelPermissions(serializers.Serializer):
    content_type = serializers.PrimaryKeyRelatedField(
        queryset=ContentType.objects.all(),
        write_only=True,
    )
    permissions = serializers.JSONField(write_only=True)

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        allowed_permissions = ["add", "change", "view", "delete"]
        permissions = validated_data["permissions"]
        for permission in permissions:
            if permission not in allowed_permissions:
                raise serializers.ValidationError(
                    {"permissions": f"'{permission}' is not an allowed value."}
                )
        return validated_data

    class Meta:
        fields = ["content_type", "permissions"]


class GroupPermissionsSerializer(serializers.ModelSerializer):
    group_permissions = ModelPermissions(many=True, write_only=True)

    class Meta:
        model = Group
        fields = ["group_permissions"]

    def update(self, instance, validated_data):
        group_permissions = validated_data["group_permissions"]
        instance.permissions.clear()
        for model_permissions in group_permissions:
            model_permissions = dict(model_permissions)
            content_type = model_permissions["content_type"]

            for permission in model_permissions["permissions"]:
                permissions = Permission.objects.get(
                    content_type=content_type,
                    codename__startswith=permission,
                )
                instance.permissions.add(permissions)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        group_permissions = instance.permissions.all()
        group_permissions_obj = {}
        for model_permissions in group_permissions:
            content_type_id = model_permissions.content_type.id
            content_type_permissions = group_permissions_obj.get(
                content_type_id,
                None,
            )
            group_permission_obj = {
                "content_type": content_type_id,
                "content_type_name": model_permissions.content_type.name,
            }
            model_permission = model_permissions.codename.split("_")[0]
            if content_type_permissions:
                group_permission_obj.update(
                    {
                        "permissions": (
                            content_type_permissions["permissions"] + [model_permission]
                        )
                    }
                )
            else:
                group_permission_obj.update({"permissions": [model_permission]})
            group_permissions_obj.update({content_type_id: group_permission_obj})

        data["group"] = instance.id
        data["group_name"] = instance.name
        data["group_permissions"] = group_permissions_obj.values()
        return data


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = "__all__"
