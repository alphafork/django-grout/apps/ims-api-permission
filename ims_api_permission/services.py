def check_auth_user_in_groups(request, groups):
    user_groups = request.user.groups.values_list("id", flat=True)
    for group in groups:
        if group in user_groups:
            return True
    return False
