from inspect import isclass

from django.apps import apps
from django.conf import settings as project_settings
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError

from ims_api_permission.models import APIGroupPermission, APIMethod, APIType


class Command(BaseCommand):
    help = "Collects managed permissions from apps."

    def add_arguments(self, parser):
        parser.add_argument("app_list", nargs="*", type=str)

    def handle(self, *args, **options):
        api_count = 0
        app_list = options["app_list"] or getattr(project_settings, "DJ_IMS_APPS")
        for app_label in app_list:
            try:
                app = apps.get_app_config(app_label)
            except LookupError:
                raise CommandError('App "%s" does not exist' % app_label)
            except AttributeError:
                pass
            else:
                api_count = self.add_custom_api_permissions(app, api_count)
        self.stdout.write(f"Added {api_count} new APIs")

    def add_custom_api_permissions(self, app, api_count):
        apis = getattr(app.module, "apis", None)
        if apis:
            for api_str in dir(apis):
                if isclass(getattr(apis, api_str)):
                    api = getattr(apis, api_str)
                    if getattr(api, "is_permissions_managed", False):
                        managed_permissions = getattr(api, "managed_permissions", None)
                        if managed_permissions:
                            methods = dict(APIMethod.METHODS).values()
                            api_type, api_created = APIType.objects.get_or_create(
                                app_label=app.label, api=api_str.lower()
                            )
                            if api_created:
                                api_count += 1
                            for method in methods:
                                api_method = APIMethod.objects.get_or_create(
                                    api_type=api_type,
                                    method=method,
                                )
                                auth_groups = managed_permissions.get(method, None)
                                if auth_groups:
                                    for group_name in auth_groups:
                                        group = Group.objects.filter(name=group_name)
                                        if not group.exists():
                                            raise CommandError(
                                                'Group "%s" does not exist' % group_name
                                            )
                                        APIGroupPermission.objects.get_or_create(
                                            api_method=api_method[0],
                                            group=group[0],
                                        )
                                        self.stdout.write(
                                            "Permission for %s on %s"
                                            % (
                                                method,
                                                api_str,
                                            )
                                            + "API given to %s group." % group_name,
                                        )
        return api_count
