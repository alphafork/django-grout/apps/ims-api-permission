from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Export group permissions."

    def add_arguments(self, parser):
        parser.add_argument("groups", nargs="*", type=str)

    def handle(self, *args, **options):
        group_list = options["groups"]

        try:
            groups = Group.objects.filter(name__in=group_list)
        except TypeError:
            raise CommandError("Groupname must be a string.")
        else:
            if len(group_list) == 0:
                groups = Group.objects.all()
            elif len(groups == 0):
                raise CommandError("Group(s) not found.")
            else:
                raise CommandError(
                    "Project has no groups. Please add group permissions and continue."
                )
        group_permissions = []
        for group in groups:
            permissions = group.permissions.values("codename")
            for permission in permissions:
                group_permissions.append(
                    {
                        **permission,
                        "group_name": group.name,
                    }
                )
        print(group_permissions)
