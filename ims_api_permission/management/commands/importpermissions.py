import argparse
import ast

from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Imports group permissions."
    missing_args_message = (
        "No file path specified. Please provide the path of permissions export."
    )

    def add_arguments(self, parser):
        parser.add_argument("group_perms", nargs=1, type=argparse.FileType("r"))

    def handle(self, *args, **options):
        group_perms = ast.literal_eval(options["group_perms"][0].read())
        permission_count = 0
        for group_perm in group_perms:
            permission_count += 1
            self.stdout.write(
                "added {} to {}".format(
                    group_perm["codename"], group_perm["group_name"]
                )
            )
            group = Group.objects.get_or_create(name=group_perm["group_name"])[0]
            try:
                permission = Permission.objects.get(codename=group_perm["codename"])
            except Permission.DoesNotExist:
                raise CommandError(
                    "Permission: %s does not exist." % group_perm["codename"]
                )
            except Permission.MultipleObjectsReturned:
                raise CommandError(
                    "Permission: %s has multiple values in db." % group_perm["codename"]
                )
            else:
                group.permissions.add(permission)
        self.stdout.write(f"{permission_count} permissions added.")
