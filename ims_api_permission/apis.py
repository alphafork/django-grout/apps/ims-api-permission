from django.contrib.auth.models import ContentType, Group
from rest_framework.permissions import BasePermission, DjangoModelPermissions
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from ims_api_permission.serializers import (
    APIGroupPermissionSerializer,
    ContentTypeSerializer,
    GroupPermissionsSerializer,
)

from .models import APIGroupPermission, APIMethod, APIType
from .services import check_auth_user_in_groups


class GenericPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        api_type = APIType.objects.filter(
            app_label=view.__module__.split(".")[0],
            api=type(view).__name__.lower(),
        )
        if api_type.exists():
            api_method = APIMethod.objects.filter(
                api_type=api_type[0],
                method=request.method,
            )
            if api_method.exists():
                api_auth_groups = APIGroupPermission.objects.filter(
                    api_method=api_method[0]
                ).values_list("group", flat=True)
                return check_auth_user_in_groups(request, api_auth_groups)
        return False


class APIMethodPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        managed_permissions = getattr(view, "managed_permissions", {})
        allowed_groups = managed_permissions.get(request.method, [])
        if request.user.groups.filter(name__in=allowed_groups):
            return True
        return False


class DjangoModelPermissionsStrict(DjangoModelPermissions):
    perms_map = {
        "GET": ["%(app_label)s.view_%(model_name)s"],
        "OPTIONS": ["%(app_label)s.view_%(model_name)s"],
        "HEAD": ["%(app_label)s.view_%(model_name)s"],
        "POST": ["%(app_label)s.add_%(model_name)s"],
        "PUT": ["%(app_label)s.change_%(model_name)s"],
        "PATCH": ["%(app_label)s.change_%(model_name)s"],
        "DELETE": ["%(app_label)s.delete_%(model_name)s"],
    }


class APIPermission(BasePermission):
    def has_permission(self, request, view):
        api_group_permissions = getattr(view, "api_group_permissions", {})
        request_method = request.method
        if api_group_permissions:
            allowed_groups = api_group_permissions[request_method]
            if request.user.groups.filter(name__in=allowed_groups):
                return True
        return False


class UserObjectPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            user = getattr(view, "get_object_user")
            allowed_methods = getattr(view, "object_user_allowed_methods")
        except AttributeError:
            return False
        else:
            if user(obj) == request.user and request.method in allowed_methods:
                return True
        return False


class APIGroupPermissionAPI(ModelViewSet):
    serializer_class = APIGroupPermissionSerializer
    queryset = APIGroupPermission.objects.all()


class GroupPermissionsAPI(ModelViewSet):
    serializer_class = GroupPermissionsSerializer
    queryset = Group.objects.all()
    http_method_names = ["get", "put", "patch", "head", "options"]


class ContentTypeAPI(ReadOnlyModelViewSet):
    serializer_class = ContentTypeSerializer
    queryset = ContentType.objects.all()
